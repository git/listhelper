#!/bin/sh
#
# This script processes a mailman moderator message.
#
# Copyright 2006, 2007, 2008, 2010, 2013, 2014, 2015, 2016, 2018, 2019
# Bob Proulx <bob@proulx.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Mailman moderator messages are structured with three mime encoded parts.
# Part 1: A top mailman introduction message.
# Part 2: The original message to the mailing list.
# Part 3: A bottom mailman control message.
#
# Replying to the 3rd part will discard the message.
#
# The process implemented here is to unpack the mime components of the
# message first into the three components.  Some components
# recursively have sub-components that are also unpacked.  These are
# put into the less than descriptive numbered msgNUM.NUM files.
#
# SpamAssassin is run on the original message payload.  If SA
# classifies the message as spam then a control message back to
# mailman is generated to discard the spam.

# Configure how spamassassin will be run.
#spamassassin="spamassassin"
#spamassassin="spamc -s 5000000 -x -d 192.168.240.11" # host desolation
spamassassin="spamc -s 5000000 -x" # localhost

# Set MAX_CROSSPOST to indicate the maximum number of times an
# identical message may be seen before it is considered spam by the
# crossassassin feature.
MAX_CROSSPOST=5
# This value is also used in honeypot-learn-spam

LC_ALL=C			# Ensure a standard sort behavior.
export LC_ALL
umask 02			# Set up for group sharing.
PATH=$PATH:/usr/sbin:/sbin	# for sendmail et al

# Work in a subdirectory.  Set up to clean up when we exit.

unset tmpdir
cleanup() {
    cd /
    test -n "$tmpdir" && rm -rf "$tmpdir" && unset tmpdir
}
trap "cleanup" EXIT
trap "cleanup; trap - HUP; kill -s HUP $$" HUP
trap "cleanup; trap - INT; kill -s INT $$" INT
trap "cleanup; trap - QUIT; kill -s QUIT $$" QUIT
trap "cleanup; trap - TERM; kill -s TERM $$" TERM
trap "trap - PIPE; cleanup; kill -PIPE $$" PIPE

tmpdir=$(mktemp -d -t mailman-filter.XXXXXXXX) || exit 1

cd $tmpdir || exit 1

# Read the file from stdin and save to a working copy of the file.
# With the working copy we can then make multiple passes.  We do make
# multiple passes currently.  This is not the performance bottleneck
# though and so has not been optimized to be avoided.

if ! cat > msg0; then
    exit 1
fi

x_beenthere=$(formail -x X-BeenThere < msg0)

# Unpack the individual mailman encoded parts into separate files.
# This will create msg1, msg2, msg3 corresponding to the three parts
# encoded by mailman.

if ! mime-unpack < msg0; then
    exit 1
fi

# The mime-unpack script unpacks mime attachments but leaves the mime
# content-type header behind as a message header.  Delete this off of
# the message.

if ! sed '1,/^$/d' msg2 > msg2.1; then
    exit 1
fi
if ! sed '1,/^$/d' msg3 > msg3.1; then
    exit 1
fi

if [ ! -s msg2.1 ]; then
    echo "Error: zero sized file in second attachment" 1>&2
    exit 1
fi

# At this point msg2.1 is the original message and msg3.1 is the
# potential discard reply message.

save_discard_reply()
{
    # extract the Message-id header.  Save the discard reply in a file
    # using the message id header as the file name.
    message_id=$(formail -x message-id < msg2.1 | tr -d '[ <>]')
    mkdir -p $HOME/var/msg-discard-db
    cp msg3.1 "$HOME/var/msg-discard-db/$message_id"
}

send_discard_reply()
{
    # Reply to the mailman message with the discard command for that md5sum.
    formail -r < msg3.1 | sendmail -t -oi
}

incr_spam_count()
{
    if [ ! -f $HOME/Mail/spamcounter ]; then
	echo 0 > $HOME/Mail/spamcounter
    fi
    expr $(cat $HOME/Mail/spamcounter) + 1 >$HOME/Mail/spamcounter
}

incr_nonspam_count()
{
    if [ ! -f $HOME/Mail/hamcounter ]; then
	echo 0 > $HOME/Mail/hamcounter
    fi
    expr $(cat $HOME/Mail/hamcounter) + 1 >$HOME/Mail/hamcounter
}

incr_mail_count()
{
    # Keep some message stats for later graphing.
    if [ ! -f $HOME/Mail/counter ]; then
	echo 0 > $HOME/Mail/counter
    fi
    expr $(cat $HOME/Mail/counter) + 1 >$HOME/Mail/counter
}

# The idea here is that if the mailman machine monty-python already
# scored the message as spam then just go with it and avoid running it
# through spamassassin again here.

run_montypython_check()
{
    if sed '/^$/q' msg0 | grep -q "^X-Spam-Checker-Version: .* on monty-python"; then
	if sed '/^$/q' msg0 | grep -q "^X-Spam-Flag: YES"; then
	    isspam=true
	    # If the spam level is 10 or greater than it is very spammy.
	    if sed '/^$/q' msg0 | grep -E -q "^X-Spam-Level: \*{10}"; then
		isveryspammy=true
	    fi
	fi
    fi
}

run_spam_check()
{
    (
	if echo "$x_beenthere" | grep -q -F -x -f $HOME/etc/language-lists; then
	    HOME=$HOME/lang
	fi
	if ! bogofilter-run -p -e < msg2.1 > msg2.3; then
	    exit 1
	fi
	bogosity=$(formail -X X-Bogosity < msg2.3)
	if [ -z "$bogosity" ]; then
	    echo "Error: Missing X-Bogosity header" 1>&2
	    exit 1
	fi
	if ! crm114-run < msg2.1 > msg2.3; then
	    exit 1
	fi
	crmstatus=$(formail -X X-CRM114-Status < msg2.3)
	if [ -z "$crmstatus" ]; then
	    echo "Error: Missing X-CRM114-Status header" 1>&2
	    exit 1
	fi
	if formail -I "$bogosity" -I "$crmstatus" < msg2.1 | $spamassassin > msg2.2; then
	    exit 1
	fi
    )
    if formail -cX X-Spam-Flag < msg2.2 | grep -q "^X-Spam-Flag: YES"; then
	isspam=true
    fi
    crmstatus=$(formail -cx X-CRM114-Status < msg2.2 | awk '{print$3}')
    crmstatus=$(printf "%.0f\n" "$crmstatus")
    if expr "$crmstatus" : '^-[0-9][0-9]*$' >/dev/null; then
	# Have seen non-spam at -14.89 before so careful.
	if [ "$crmstatus" -le -20 ]; then
	    isspam=true
	fi
    fi

    if formail -cx X-Spam-Status < msg2.2 | grep -q USER_IN_WHITELIST_TO; then
	istugwhitelisted=true
    else
	if formail -cX X-Bogosity: < msg2.2 | grep -q "^X-Bogosity: Spam" \
		&& formail -cX X-CRM114-Status: < msg2.2 | grep -q "^X-CRM114-Status: SPAM" \
		&& formail -cx X-Spam-Status: < msg2.2 | grep -q "tests=.*BAYES_9"; then
	    : all learning engines agree it is spam therefore it is spam
	    isspam=true
	fi
	if $isspam; then
	    # This message is spam.  Cache the message digest for future use.
	    mkdir -p $DIGEST_DIR
	    if [ ! -f $DIGEST_DIR/$DIGEST_FILE ]; then
		echo 0 >$DIGEST_DIR/$DIGEST_FILE
	    fi
	    expr $(cat $DIGEST_DIR/$DIGEST_FILE) + 10 >$DIGEST_DIR/$DIGEST_FILE
	    # # If the spam level is 10 or greater than it is very spammy.
	    # if sed '/^$/q' msg2.2 | grep -E -q "^X-Spam-Level: \*{10}"; then
	    #     isveryspammy=true
	    # fi
	fi
    fi
}

filter_body()
{
    sed '1,/^$/d' msg2.1
}

check_msg_contents()
{
    if ! filter_body | cat -s | grep -q .; then
	: empty message
	isspam=true
	isveryspammy=true
	return
    fi
    if grep -q -E -f $HOME/etc/blacklist-body msg2.1; then
	: Matched known spam in message body.
	isspam=true
	isveryspammy=true
	return
    fi
    if grep -q -E -f $HOME/etc/whitelist-body msg2.1; then
	: Matched known non-spam in message body.
	isspam=false
	isveryspammy=false
	whitelisted=true
	return
    fi
}

set_msg_digests()
{
    if [ -z "$DIGEST_DIR" ]; then
	DIGEST=$(filter_body | md5sum | awk '{print$1}')
	DIGEST_DIR=$HOME/var/digests/$(echo $DIGEST | sed 's/^\(..\).*/\1/')
	DIGEST_FILE=$(echo $DIGEST | sed 's/^..//')
    fi
}

check_msg_digests()
{
    set_msg_digests
    if [ -f $DIGEST_DIR/$DIGEST_FILE ]; then
	if [ $(cat $DIGEST_DIR/$DIGEST_FILE) -gt $MAX_CROSSPOST ]; then
	    isspam=true
	fi
    fi
}

incr_msg_digests()
{
    set_msg_digests
    mkdir -p $DIGEST_DIR
    if [ ! -f $DIGEST_DIR/$DIGEST_FILE ]; then
	echo 0 >$DIGEST_DIR/$DIGEST_FILE
    fi
    expr $(cat $DIGEST_DIR/$DIGEST_FILE) + 1 >$DIGEST_DIR/$DIGEST_FILE
}

incr_mail_count

isspam=false
isveryspammy=false
whitelisted=false
istugwhitelisted=false

check_msg_contents

if ! $isspam && ! $whitelisted; then
    check_msg_digests
fi

if ! $isspam && ! $whitelisted; then
    run_montypython_check
fi

if ! $isspam && ! $whitelisted; then
    run_spam_check
fi

if ! $whitelisted; then
    incr_msg_digests
fi

if $isspam; then
    FOLDER=$HOME/Mail/caughtspam
    send_discard_reply
    incr_spam_count
    if $isveryspammy; then
	# If the message is very spammy they take it straight to the archive
	# so that humans won't need to review it.
	FOLDER=$HOME/Mail/caughtspam-archive
    fi
else
    save_discard_reply
    incr_nonspam_count
    if $whitelisted; then
	FOLDER=$HOME/Mail/OLD/caughtham
    elif $istugwhitelisted; then
	FOLDER=$HOME/Mail/caughtham-tug
    else
	# emacs-orgmode was okay with the automated discards but did
	# not want listhelper humans doing any additional discards.
	if formail -cx Received < msg2.1 | grep -q "emacs-orgmode"; then
	    FOLDER=$HOME/Mail/caughtham-fsf
	else
	    FOLDER=$HOME/Mail/caughtham
	fi
    fi
fi

if [ -f msg2.2 ]; then
    mkdir -p $FOLDER/tmp $FOLDER/new $FOLDER/cur
    chmod a+r $FOLDER/new/$(safecat $FOLDER/tmp $FOLDER/new < msg2.2)
fi

FOLDER=${FOLDER}-mm
mkdir -p $FOLDER/tmp $FOLDER/new $FOLDER/cur
chmod a+r $FOLDER/new/$(safecat $FOLDER/tmp $FOLDER/new < msg0)

exit 0
